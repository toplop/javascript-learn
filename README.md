# Practice JavaScript
---
Este proyecto abarcara los temas siguientes de la ruta de aprendizaje de ```FullStack React & Flutter```:
* ```Full JavaScript```
* ```TypeScript```
* ```Buenas practicas de código```
* ```Postman Project```
* ```TypeScript with MySQL```
* ```Git y GitFlow```
* ```Migración a MongoDB```
* ```ES```
---
## ¿En qué consiste?
Se plantea crear un API RestFul con Express y NodeJS, donde un usuario que haga una petición a nuestro backend, pueda consultar los productos, el preció y si hay existencias, pero únicamente podrá agregar al carrito si se encuentra auntetificado.

Los productos seran agregados por categorias y sub categorias, por ejemplo:
**Componentes de PC --- Procesadores**, estos de prodcutos deberan contar con una imagen, la cuál sera almacenada el servidor, el detalle del producto deberia visualizarse al mandar como ```parametro en la URL el Id```

Cuando se quiera consultar un producto este debe contar con la posibilidad de mandar filtros los cuales son **Precio - mayor a menor**, **Precio - menor a mayor**, **Categoria** y **SubCategoria** con la posibilidad de enviar dos de estos por ejemplo **Precio - mayor a menor y Categoria** si no se envian por defecto sera **Por fecha de ingreso**, la fecha de ingreso se actualiza, cada vez que ingresan nuevos productos o cantidad de un producto que ya existe, los 2 filtros deberian enviarse mediante un ```Query en la URL```.

En las opciones de ```Query también debe existir la posibilidad de pedir un limite de productos``` esto como metodo practico para generar una paginación, por este motivo las categorias y subcategorias deben tener la cantidad de productos que tiene cada una.

Al añadir el carrito existen ciertas validaciones, si volvemos a agregar un producto al carrito, este solo aumentara la cantidad que existen en el carrito, cuando el usuario este satisfecho y le de a comprar en el carrito, este le pedira datos de envio y facturación, al complentar los mismos y que la compra sea exitos el backend generara una factura, en PDF y en la base de datos con la información ademas de enviar la misma por correo electronico al usuario.

---

**El proyecto se dividira en las fases que se consideren necesarias.**

**Constaran de una parte de investigación de la cúal no se hablo en clases de ningún tipo**

---
### FASE 1
Esta fase evalua JavaScript en su totalidad, se debera clonar el proyecto y crear una rama con la siguiente estructura ```fase1/[Nombre del challenger]```, al momento de crear la rama se comenzara el trabajo en la misma y al finalizar se hara un pull request para mezclar a master.

En esta fase se creara la funcionalidad de CRUD de Productos, Categorias y SubCategorias, ademas de la consulta con sus respectivos filtros, deberan estar en su totalidad cada uno y la mezcla de los mismos.
en la parte de borrado, si se borra una categoria, todas las subcategorias pasaran a vincularse a una categoria por defecto, si se borra una subcategoria todos los productos de esa subcategoria pasaran a vincularse a una subcategoria por defecto, cabe mencionar que se manejaran un active
y unactive en todas las entidades, es decir se puede borrar e inactivar, al traer los productos, categorias o subcategorias solo vendran las que esten activas, las que no se traeran con una opción en un query donde se mandara **unactive=true**.

***En Notion se creo una parte de [Learning](https://www.notion.so/Learning-27736e5acc1e41c1a610213ed45d994d) donde estan sus nombres, ahi deberan documentar todo lo que se haga durante este proyecto de tal manera que únicamente leyendo eso pueda realizar toda la funcionalidad de su API***.

#### INVESTIGACION
Los productos deberian contar con la posibilidad de tener imagenes, por lo mismo se debera investigar sobre ```Multer``` un ```Middleware``` para la subida de imagenes ademas de ```Servir archivos estaticos con Express``` para poder adjuntar la URL de las imagenes, se debera implementar ```MySQL con NodeJS``` esta inveestigación es más compleja, porque se debera buscar abtraer la lógica lo suficiente para poder migrar posteriormente a ```MongoDB``` Asi que el codereview de esto sera sumamente minusioso.